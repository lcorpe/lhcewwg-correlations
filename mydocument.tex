%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS paper.
%-------------------------------------------------------------------------------
% \pdfoutput=1
% The \pdfoutput command is needed by arXiv/JHEP/JINST to ensure use of pdflatex.
% It should be included in the first 5 lines of the file.
% \pdfinclusioncopyfonts=1
% This command may be needed in order to get \ell in PDF plots to appear. Found in
% https://tex.stackexchange.com/questions/322010/pdflatex-glyph-undefined-symbols-disappear-from-included-pdf
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\newcommand*{\ATLASLATEXPATH}{latex/}
% Use this variant if the files are in a central location, e.g. $HOME/texmf.
% \newcommand*{\ATLASLATEXPATH}{}
%-------------------------------------------------------------------------------
%\documentclass[PAPER, atlasdraft=false, texlive=2016, UKenglish]{\ATLASLATEXPATH atlasdoc}
\documentclass[PAPER, atlasdraft=false, texlive=2016, UKenglish]{article}
% The language of the document must be set: usually UKenglish or USenglish.
% british and american also work!
% Commonly used options:
%  atlasdraft=true|false This document is an ATLAS draft.
%  texlive=YYYY          Specify TeX Live version (2016 is default).
%  coverpage             Create ATLAS draft cover page for collaboration circulation.
%                        See atlas-draft-cover.tex for a list of variables that should be defined.
%  cernpreprint          Create front page for a CERN preprint.
%                        See atlas-preprint-cover.tex for a list of variables that should be defined.
%  NOTE                  The document is an ATLAS note (draft).
%  PAPER                 The document is an ATLAS paper (draft).
%  CONF                  The document is a CONF note (draft).
%  PUB                   The document is a PUB note (draft).
%  BOOK                  The document is of book form, like an LOI or TDR (draft)
%  txfonts=true|false    Use txfonts rather than the default newtx
%  paper=a4|letter       Set paper size to A4 (default) or letter.

%-------------------------------------------------------------------------------
% Extra packages:
\usepackage{\ATLASLATEXPATH atlaspackage}
% Commonly used options:
%  biblatex=true|false   Use biblatex (default) or bibtex for the bibliography.
%  backend=bibtex        Use the bibtex backend rather than biber.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{\ATLASLATEXPATH atlasbiblatex}

% Useful macros
\usepackage{\ATLASLATEXPATH atlasphysics}
% See doc/atlas_physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, heppparticle, hepprocess, hion, jetetmiss, math, process, other, texmf
% See the package for details on the options.

% Files with references for use with biblatex.
% Note that biber gives an error if it finds empty bib files.
% \addbibresource{mydocument.bib}
\addbibresource{bib/ATLAS.bib}
\addbibresource{bib/CMS.bib}
\addbibresource{bib/ConfNotes.bib}
\addbibresource{bib/PubNotes.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

% Add you own definitions here (file mydocument-defs.sty).
\usepackage{mydocument-defs}
\usepackage{listings}
\usepackage{color}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
  backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\small,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

%-------------------------------------------------------------------------------
% Generic document information
%-------------------------------------------------------------------------------

% Title, abstract and document 
\input{mydocument-metadata}
% Author and title for the PDF file
\hypersetup{pdftitle={LHCEWWG-Document},pdfauthor={LHC Eelectroweak Working Group - Jets and Bosons}}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------
\begin{document}

\maketitle

\tableofcontents

%-------------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
%-------------------------------------------------------------------------------
Comparison of experimental measurements and SM predictions in the presence of bin-to-bin correlated uncertainties is an increasingly important topic in the high energy physics community.
For example, at present the agreement of a generated distribution of an observable with its measured distribution is typically made by looking at the ratio between the data and simulation in each bin. So far this approach has been acceptable since the uncertainties would be dominated by the statistical component, which is often uncorrelated between bins. As the LHC collects an increasingly large dataset however, this simple ratio becomes unsuitable, since the effect of correlations of systematic uncertainties between bins becomes important.
Similarly, re-interpretation of searches for new physics need to account for correlations between bins to be valid.
In both cases, analysts need access to covariance information from measurements published by experimentalists.
At present, there is no specific agreement on how this information should be stored and propagated. This note provides some recommendations on how such correlation information should be stored and attempts to implement some of the suggestions.


\section{Current workflow to preserve analysis results}

The most common way to preserve the results of a measurement from an LHC experiment it to store the result on HEPData~\cite{Maguire:2017ypu}.
The HEPData website contains listings for individual papers, where the information from the tables and figures may be stored in a database.
In some cases, additional information, which is not available in the main body of the paper, may also be stored on HEPData.

Going hand in hand with HEPdata entries are Rivet~\cite{Buckley:2010ar} routines which define the fiducial region(s) in which a measurement was made. 
Rivet is a generator-independent programme, where truth events can be processed to show the truth-level distributions resulting from measurements and compared to the reference data from the paper in a dedicated human-readable format, called YODA. In this way, new generator predictions may be run through an Rivet analysis plugin (this is called a routine) and the result may be compared to the experimental data directly (assuming that the data was corrected for detector effects). Rivet also incorporates detector smearing functionality so it may also be used for search results and detector-level measurements.

HEPData and Rivet are intended to be synchronised so that they contain identical information: data from HEPData listings may be exported to Rivet's YODA format and these outputs should be identical to the reference data which comes bundled with Rivet. 

The HEPdata-Rivet workflow allows for an efficient preservation of experimental measurements, especially if they are unfolded to particle level, since it avoids the need to run expensive detector reconstruction and simulation of detector effects. Rivet is commonly used by the generator development community for validation, generator comparison and checking whether new features improve agreement with experimental data. Furthermore, Rivet can also be used for re-interpretation via tools like CONTUR~\cite{Butterworth:2016sqg}.

For searches, re-interpretation may be done via simplified likelihoods~\cite{Buckley:2018vdr} (which rely on covariance matrices, usually provided directly by the analysis team). In this case, covariance information also needs to be stored on HEPData.


\section{How to add covariance information into the workflow}

Whether dealing with preservation of results from measurements or searches, at particle-level or detector-level, it is clear any convention to store covariance information must be implemented both in HEPdata and Rivet, since these tools are key elements in the workflow for analysis re-use. In order to preserve the covariance information from an analysis, two options are available: either a covariance matrix must be provided directly by the analysis team; or a breakdown of all uncertainties in each bin of measured distributions should be provided.

Each option has pros and cons, but in the general the uncertainty breakdown is preferred since it can be trivially converted into a covariance matrix using linear algebra (two methods exist, toys-based or direct, which are detailed in Appendix~\ref{methods}), but the reverse operation is not trivial. Furthermore, this approach allows the covariance matrix to be built across several distributions of a given measurement, or even different measurements, so long as the same naming conventions for each source of uncertainty is respected. 

In principle, a covariance matrix can also be uploaded to HEPData and propagated to a 2-dimensional histogram in YODA. Although there is no agreed format to do this at present, this workflow is possible.  However, until recently, although HEPData was able to handle uncertainty breakdown in a given measurement, this information could not be propagated to Rivet since no format within YODA existed to store this breakdown. Recent technical improvements have made this possible, which are detailed below.


\section{Summary of recent and upcoming technical developments}

\subsection{A format to store error breakdowns in HEPdata entries}

HEPData was re-written in 2015-2016, and many technical improvements were implemented. Among these, the ability of uploading an arbitrary number of label to each histogram which represent individual sources of uncertainty. The total uncertainty shown on the HEPData site is taken to be the sum in quiadrature of the individual components.
The upload format for HEPdata is a YAML file, where each figure, distribution or table is associated to a YAML entry.
The HEPdata YAML format is detailed here~\cite{HEPDataGithub} and a minimal example of the format given here for reference. 
%\begin{lstlisting}[language=Python]
%print 'hello world'
%\end{lstlisting}
\lstinputlisting[language=Python]{exampleHisto.yaml}

The YAML entry for a given histogram consists of a list of independent variables (e.g. the $x$-axis in a distribution, or the $x$- and $y$-axes for a heat map), in this case $H_{T}$, and dependent variables (e.g. the $y$-axis, or $z$-axis respectively), in this case $d\sigma / d(H_{T})$. The values for the independent and dependent variables can either be bin ranges, as in this example or central values of points.  The dependent variable values for each bin are given by the \texttt{value} field, but can also be assigned a dictionary of uncertainties for each point, in the \texttt{errors} field, where the keys of the dictionary are the names of the sources of uncertainty.

HEPData YAML files support symmetric uncertainties (labelled \texttt{symerror}) or asymmetric uncertainties (labelled \texttt{asymerror}) where values for the updwards and downwards variations (denoted as \texttt{plus} and \texttt{minus} respectively) are given separately.
HEPdata supports relative uncertainties (which are indicated with a '\%' sign), or absolute uncertainties.
The uncertainties can be signed. Positive uncertainties are understood to represent increases in the yield or cross-section or value in that bin or point, while negative uncertainties are taken to mean cases where the value is reduced. 

\subsection{The YODA format, and improvements to store error breakdowns}

Rivet's custom data format, YODA, was designed to be a lightweight human-readable framework for computational data handling. The same example as described above in the YODA format looks like:
\lstinputlisting[language=Python]{HEPData-1560189284-v1-yoda.brkdn.yoda}

This example was produced by creating a dummy HEPData entry for the above example, and converting to YODA from HEPdata. For older version of HEPdata and YODA, the information regarding the error breakdown would have been lost, with only the total error reported. As of Rivet version 2.6.2 (YODA 1.7.1.) this is possible, and additional feature was introduced to store the uncertainties in YAML annotations to the YODA histograms. The YAML annotation is a set of nested dictionaries which are organised in the following way:

\begin{lstlisting}
ErrorBreakdown: 
 {
   0:  {
        'stat,Data_statistics': {dn: -2.5347e-06, up: 2.5347e-06}, 
        'sys,DY_TH_Scale_uncertainty': {dn: -9.6354e-08, up: 9.6354e-08}, 
        'sys,JET_GroupedNP_1': {dn: -9.6549e-07, up: 9.6549e-07}
       }, 
    1: {
        'stat,Data_statistics': {dn: -8.6171e-06, up: 8.6171e-06}, 
        'sys,DY_TH_Scale_uncertainty': {dn: -8.4579e-07, up: 8.4579e-07}, 
        'sys,JET_GroupedNP_1': {dn: -8.6178e-06, up: 8.6178e-06}
       }, 
    2: {
        'stat,Data_statistics': {dn: -2.6374e-06, up: 2.6374e-06}, 
        'sys,DY_TH_Scale_uncertainty': {dn: -4.6791e-07, up: 4.6791e-07}, 
        'sys,JET_GroupedNP_1': {dn: -6.7696e-06, up: 6.7696e-06}
       }, 
    3: {
        'stat,Data_statistics': {dn: -2.0552e-06, up: 2.0552e-06}, 
        'sys,DY_TH_Scale_uncertainty': {dn: -1.3018e-07, up: 1.3018e-07}, 
        'sys,JET_GroupedNP_1': {dn: -4.9286e-07, up: 4.9286e-07}
       }
  }
\end{lstlisting}.

Each bin is referred to by it's index in the annotation, and is associated to a dictionary. The keys of these dictionaries are the labels of each source of uncertainty. In the example above, we consider three sources. For each source of uncertainty in each bin, a further dictionary stores the relative uncertainty in the upwards (\texttt{up}) and downwards(\texttt{dn}) variation.

The advantage of this format is that the uncertainty breakdown for a given measurement is associated directly to the relevant YODA reference histogram. There is no need to link the reference data histogram to any other entry or table to find out the covariance matrix (this is of course not the case if a covariance matrix is stored as a separate object).

\subsection{Seamless propagation of covariance information from HEPData to Rivet/YODA}

Until the aforementioned format for uncertainty breakdowns was introduced into YODA, HEPdata did not propagate any uncertainty breakdown information when exporting an entry into YODA format. Instead, the individual uncertainties which were uploaded on HEPdata were summed in quadrature to give a single total uncertainty, which was then propagated to YODA.
However, since the release of YOPD 1.7.1, when a HEPdata entry is exported to YODA format, the uncertainty breakdown is propagated into the format mentioned in the previous section by default. 


\subsection{A common library of tools to manipulate YODA files with covariance information}

A library of tools to manipulated YODA objects which contain an uncertainty breakdown in prodided along with this document, and can be found in~\cite{correlations-library}. Some examples of the functionality provided in the library are detailed below:
\begin{itemize}
\item constructing a covariance matrix from the error breakdown using the toys method 
\item constructing a covariance matrix from the error breakdown using the direct method 
\end{itemize}

\section{Recommendations and proposed workflow}

In order to store covariance information from measuremenets in a standardized way, the following conventions are suggested:

\begin{enumerate}

\item Analysis results, in particular measurements made a particle-level, should be uploaded to HEPdata, and should include the full breakdown of uncertainties in each bin.. This typically means that the error breakdown in each bin and each distribution should be available to the collaboration during review;
\item If an uncertainty has several components, for example the Jet Energy Scale, then each component should be listed separately in the breakdown;
\item Uncertainties which are not correlated between bins (for example, stat uncertainties in most case) should be stored with a prefix \texttt{stat,} or \texttt{uncorr,} at the beginning of their label. Conversely, uncertainties which should be correlated between bins should be labelled with a prefix \texttt{sys,} or \texttt{corr,};
\item Only errors which are not correlated between bins should ideally be uploaded as asymmetric uncertainties.  If a \texttt{sys,} error is uploaded as a symmetric error, then it will be assumed that the upward variation increases the value of the point;
\item Uncertainties can be specified as relative or absolute, as preferred by the analysts;
\item Where possible, the exact ATLAS or CMS naming conventions should be propagated without change (aside from the \texttt{sys,} or \texttt{stat,} prefix) for uncertainties which are common between measurements. This would make it possible in principle to correlate the results of several measurements in the future;
\item A Rivet routine should be provided for all CMS and ATLAS particle-level measurements, so that the fiducial region is well defined, and the reference data should be synchronised with the corresponding HEPData entry;
\item If for whatever reason it is not possible to follow the above conventions, a covariance matrix may be provided directly as a HEPData table. The correspondence between each row/column of the able and the relevant distribution should be described clearly. The HEPData authors are working on a more programmatic solution to assign a row of a table to a bin of a histogram, but this is not available yet;
\item If an analysis wishes to store the statistical covariance information as a series of \texttt{Bootstrap} histograms, then these should be attached to the additional material of the HEPdata entry;

\end{enumerate}


\section{Step-by-step example}
\subsection{Uploading Error breakdowns to HEPData}
\subsection{Uploading covariance matrices to HEPData}
\subsection{Propagating from HEPData to YODA}
\subsection{Estimating goodness of fit between data and simulation in Rivet}

\section{Outlook and next steps}




%-------------------------------------------------------------------------------
\clearpage
\appendix
\part*{Appendix}
\addcontentsline{toc}{part}{Appendix}

%-------------------------------------------------------------------------------
\section{Pseudocode examples}
\label{method}
\subsubsection{Error breakdown to covariance matrix : Direct method}
\begin{lstlisting}[language=Python]

def makeCovarianceMatrix( ao, ignore_corrs=False ):
   """
   `ao` Yoda AO  
   `ignore_corrs` Bool (option to ignore correlations and treat everything as uncorrelated between bins)  

   Build the covariance matrix for an AO with the direct method treating error sources whose  
   label contains 'stat' as uncorrelated between bins and others as correlated between bins.  
   Fast but needs symmetric errors.
   """
   nbins = ao.numPoints
   corr = ao.annotation('ErrorBreakdown') if '1.7' in yoda.version() else yaml.load(ao.annotation('ErrorBreakdown'))
   dummyM = np.outer(range(nbins), range(nbins) )
   covM = np.zeros(dummyM.shape)
   if len (corr)==0 : 
     for ibin in range (nbins):
       covM[ibin][ibin]= ((ao.points[ibin].yErrs[0]+ao.points[ibin].yErrs[1])/2)**2
       if covM[ibin][ibin]==0 :  covM[ibin][ibin]==1
     print "[WARNING], ao ", ao.path , " has no errors. Setting cov martix to unit... but consider excluding it !"
     return covM
   systList= corr[0].keys()
   totErr = np.zeros(nbins)
   for sname in systList:
     systErrs = np.zeros(nbins)
     for ibin in range (nbins):
       nomVal =  ao.points[ibin].y
       systErrs[ibin]=((abs(corr[ibin][sname]['up']))+(abs(corr[ibin][sname]['dn'])))*0.5 #up/dn are considered the same since NoToys assumes symmetric errors
       #systErrs[ibin]=((abs(corr[ibin][sname]['up']))) #up/dn are considered the same since NoToys assumes symmetric errors
     if (ignore_corrs or 'stat' in sname):
        covM += np.diag(systErrs*systErrs)
     else:
        covM += np.outer(systErrs,systErrs)
   if LA.det(covM)==0:
     printMatrix(covM)
     print "[ERROR], the above cov matrix is singular for ", ao.path, " ! exit"
     print "( if this is a cov matrix for a super-AO, perhaps you have several highly correlated distributions in your set. Try vetoing some.)"
     exit(1)
   return covM

\end{lstlisting}
\subsubsection{Error breakdown to covariance matrix : Pseudo-experiments (toys) method}
\begin{lstlisting}[language=Python]
def makeCovarianceMatrixFromToys( ao , ntoys=10000, ignore_corrs=False):
   """
   `ao` Yoda AO 
   `ntoys` int (number of toys to throw)
   `ignore_corrs` Bool (option to ignore correlations and treat everything as uncorrelated between bins)  

   Build the covariance matrix for an AO with the toys method treating error sources whose  
   label contains 'stat' as uncorrelated between bins and others as correlated between bins.  
   Slow, depends on ntoys, but can use asymmetric errors.  
   """
   nbins = ao.numPoints
   corr = ao.annotation('ErrorBreakdown') if '1.7' in yoda.version() else yaml.load(ao.annotation('ErrorBreakdown'))
   systList= corr[0].keys()
   toys  = np.zeros( [ ntoys, nbins ] )
   
   for itoy in range( ntoys ):
      shift = np.zeros( len(systList) )
      delta = np.zeros( nbins )
      isyst = 0
      for sname in systList:
         if (ignore_corrs or 'stat' in sname):
            for n in range(nbins):
               l = np.random.normal(0,1) #bin-bin independent stat fluctuations
               s = ((corr[n][sname]['up'])) if l >= 0. else ((corr[n][sname]['dn'])) 
               delta[n] += abs(l) * s
         else:
            l = np.random.normal(0,1)
            shift[isyst] = l #bin-bin correlated fluctuations

            for n in range(nbins):
               s = ((corr[n][sname]['up'])) if l >= 0. else ((corr[n][sname]['dn'])) 
               delta[n] += abs(l)  * s # linear interpolation
         isyst+=1

      for n in range(nbins):
         toys[itoy][n] =  ( 1. + delta[n] )

   cov = np.cov( toys, rowvar=0 )
   if LA.det(cov)==0:
     printMatrix(cov)
     print "[ERROR], the above cov matrix is singular for ", ao.path, " ! exit"
     print "( if this is a cov matrix for a super-AO, perhaps you have several highly correlated distributions in your set. Try vetoing some.)"
     exit(1)
   return cov 
\end{lstlisting}

%-------------------------------------------------------------------------------


\printbibliography
% Auxiliary material - comment out the following line if you do not have any
%\include{mydocument-auxmat}
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Extra tables etc. for HepData - comment in the following line if you have any
% \include{mydocument-hepdata}
%-------------------------------------------------------------------------------

\end{document}
